package edu.tcs.registration.utilities;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
	private static ServiceRegistry registry;
	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				
				
				
				//Create configuration
				Configuration configuration=new Configuration();
				configuration.configure("hibernate.cfg.xml");
				
				
				// Create SessionFactory
				sessionFactory = configuration.buildSessionFactory();	

			} catch (Exception e) {
				e.printStackTrace();
				}

		}
		return sessionFactory;
	}

	public static void shutdown() {
		if (registry != null) {
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}
}