package edu.tcs.registration.repository;

import edu.tcs.registration.model.Employee;
import edu.tcs.registration.utilities.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;


public class EmployeeRepository {
	Transaction transaction =null;

	public List<Employee> consultEmployees() {
		List<Employee> allEmployee = new ArrayList<Employee>();
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			
			//Start transaction
			transaction = session.beginTransaction();
			//Getting employees info
			allEmployee = (List<Employee>)session.createQuery("from employee_data").list();
			//Commit transaction
			transaction.commit();
		
		}  catch (Exception e) {
	         if (transaction != null) {
	             transaction.rollback();
	          }
	          e.printStackTrace();
	       } 
		HibernateUtil.shutdown();
		return allEmployee;
	
			}
	
	public void updateEmployeeInfo(Employee employee) {
		
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			
			//Start transaction
			transaction = session.beginTransaction();
			//Save employee object
			session.update(employee);
			transaction.commit();
			System.out.println("Employee: "+employee.getFirst_name()+" has been updated");
		}  catch (Exception e) {
	         if (transaction != null) {
	             transaction.rollback();
	          }
	          e.printStackTrace();
	       } 
		HibernateUtil.shutdown();
	}
	
	public void addEmployee(Employee employee) {
		
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			
			//Start transaction
			transaction = session.beginTransaction();
			//Save employee object
			session.save(employee);
			transaction.commit();
			System.out.println("Employee: "+employee.getFirst_name()+" has been added");
		}  catch (Exception e) {
	         if (transaction != null) {
	             transaction.rollback();
	          }
	          e.printStackTrace();
	       } 
		HibernateUtil.shutdown();
	}
	
	public void deleteEmployee(Long id) {

	try(Session session = HibernateUtil.getSessionFactory().openSession()){
			
			//Start transaction
			transaction = session.beginTransaction();
			//Delete employee
			Employee employee = session.get(Employee.class,id);
			session.delete(employee);
			//Commit transaction
			transaction.commit();
			System.out.println("Employee with: "+id+" has been delete");
		}  catch (Exception e) {
	         if (transaction != null) {
	             transaction.rollback();
	          }
	          e.printStackTrace();
	       } 
		HibernateUtil.shutdown();
		
	}
	
	
}
