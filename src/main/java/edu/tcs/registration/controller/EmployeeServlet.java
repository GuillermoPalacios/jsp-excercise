package edu.tcs.registration.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.tcs.registration.model.Employee;
import edu.tcs.registration.repository.EmployeeRepository;

@WebServlet(name = "register", urlPatterns = {"/register"})
public class EmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

 
    public EmployeeServlet() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		RequestDispatcher dispatcher= request.getRequestDispatcher("/html/RegisterForm.jsp");
		dispatcher.forward(request, response);
	}

	//Adding a new user
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 EmployeeRepository employeeRepository = new EmployeeRepository();
		//Getting parameters from the jsp form
		String first_name=request.getParameter("firstName");
		String last_name=request.getParameter("lastName");
		String password=request.getParameter("password");
		String address=request.getParameter("address");
		String phone_number=request.getParameter("phone_number");
		String email=request.getParameter("email");
		
		//Setting parameters in order to make some actions
		Employee employee = new Employee();
		employee.setFirst_name(first_name);
		employee.setLast_name(last_name);
		employee.setPassword(password);
		employee.setAddress(address);
		employee.setPhone_number(phone_number);
		employee.setEmail(email);

		employeeRepository.addEmployee(employee);
		//I had a lot of troubles trying to redirect my page
	     RequestDispatcher requestDispatcher = request
                 .getRequestDispatcher("/html/EmployeeList.jsp");
         requestDispatcher.forward(request, response);
	
		
	}

}
